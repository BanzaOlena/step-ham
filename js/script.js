/** @format */

"use strict";

//  section with tabs

const servisesMenu = document.querySelector(".servises-menu");

servisesMenu.addEventListener("mouseup", (event) => {
  let target = event.target;

  let dataTarget = target.getAttribute("data-tab");

  let description = document.getElementById(dataTarget);

  let item = document.querySelector(".description-item-active");

  if (servisesMenu.querySelector(".servis-item-active") !== null) {
    servisesMenu
      .querySelector(".servis-item-active")
      .classList.remove("servis-item-active");

    item.classList.remove("description-item-active");
  }
  if (target.closest(".servises-menu")) {
    target.classList.add("servis-item-active");
    description.classList.add("description-item-active");
  }
});

//  Filter portfolio

const menuWorks = document.querySelector(".menu-works");

menuWorks.addEventListener("click", (event) => {
  const portfolioItem = document.querySelectorAll(".portfolio-item");
  let target = event.target;
  let filterName = target.getAttribute("data-filter");

  if (menuWorks.querySelector(".work-item-active") !== null) {
    menuWorks
      .querySelector(".work-item-active")
      .classList.remove("work-item-active");
  }

  if (target.closest("LI")) {
    target.classList.add("work-item-active");
    portfolioItem.forEach((element) => {
      element.classList.remove("hidden");
      if (element.dataset.filter !== filterName && filterName !== "all") {
        element.classList.add("hidden");
      }
    });
  }
});

//  Add new photo in portfolio

const load = document.querySelector(".load");

load.addEventListener("click", preLoad);

let courent = 0;

function preLoad() {
  let elemLi = document.querySelector(".portfolio-item");

  let portfolioList = document.querySelector(".portfolio-list");

  let fragment = document.createDocumentFragment();

  for (let index = 1; index <= 12; index++) {
    const itemLi = elemLi.cloneNode(true);
    itemLi.classList.add("hiden");

    if (index > 3 && index <= 6)
      itemLi.setAttribute("data-filter", "wordpress");

    if (index > 6 && index <= 9)
      itemLi.setAttribute("data-filter", "graphicDesig");

    if (index > 9 && index <= 12)
      itemLi.setAttribute("data-filter", "landingPages");

    fragment.append(itemLi);
  }

  let image = fragment.querySelectorAll(".portfolio-image");
  let hiddenText = fragment.querySelectorAll(".hidden-text");

  for (let e = 0; e < hiddenText.length; e++) {
    if (e > 2 && e <= 5) {
      hiddenText[e].innerText = "Wordpress";
    }

    if (e > 5 && e <= 8) {
      hiddenText[e].innerText = "Graphic Design";
    }

    if (e > 8 && e <= 11) {
      hiddenText[e].innerText = "Landing Pagess";
    }
  }

  for (let i = 0; i < image.length; i++) {
    image[i].src = `./image/load/load-${i}.jpg`;
  }
  courent++;

  if (courent === 2) {
    for (let i = 0; i < image.length; i++) {
      image[i].src = `./image/load/load-${i + 12}.jpg`;
      load.remove();
    }
  }
  portfolioList.append(fragment);

  const activeCatagory = document.querySelector(".work-item-active");
  const dataFilter = activeCatagory.dataset.filter;
  const portfolioItem = document.querySelectorAll(".portfolio-item");

  portfolioItem.forEach((elem) => {
    if (elem.dataset.filter === dataFilter || dataFilter === "all") {
      elem.classList.remove("hidden");
    }
  });
}

//  Slider

const sliderDiv = document.querySelector(".slider");

let courentIndex = 1;
sliderDiv.addEventListener("click", (event) => {
  const sliderItem = document.querySelectorAll(".reviewer-slider");

  let target = event.target;

  if (target.closest(".slider")) {
    if (target.classList.contains("move-left")) {
      if (courentIndex === 0) {
        sliderItem[courentIndex].classList.remove("reviewer-slider-active");

        courentIndex = sliderItem.length - 1;
        sliderItem[courentIndex].classList.add("reviewer-slider-active");

        setCurrentMain();
        return;
      }
      sliderItem[courentIndex].classList.remove("reviewer-slider-active");
      courentIndex--;
      sliderItem[courentIndex].classList.add("reviewer-slider-active");
    }
    if (target.classList.contains("move-right")) {
      if (courentIndex === sliderItem.length - 1) {
        sliderItem[courentIndex].classList.remove("reviewer-slider-active");
        courentIndex = 0;
        sliderItem[courentIndex].classList.add("reviewer-slider-active");
        setCurrentMain();
        return;
      }
      sliderItem[courentIndex].classList.remove("reviewer-slider-active");
      courentIndex++;
      sliderItem[courentIndex].classList.add("reviewer-slider-active");
    }
  }
  if (target.closest("LI")) {
    sliderItem.forEach((el) => {
      el.classList.remove("reviewer-slider-active");
    });
    target.closest("LI").classList.add("reviewer-slider-active");

    for (let i = 0; i < sliderItem.length; i++) {
      if (sliderItem[i].classList.contains("reviewer-slider-active")) {
        courentIndex = i;
        break;
      }
    }
  }
  setCurrentMain();
});

function setCurrentMain() {
  const reviewer = document.querySelectorAll(".reviewer");
  const dataName = document.querySelector(".reviewer-slider-active").dataset
    .name;
  const targetName = document.getElementById(dataName);
  reviewer.forEach((el) => {
    el.classList.remove("reviewer-active");
  });
  targetName.classList.add("reviewer-active");
}

//  Gallery with Masonry

const gallery = document.querySelector(".gallery-wrap");
const msnr = new Masonry(gallery, {
  itemSelector: ".gallery-item",
  columnWidth: 370,
  gutter: 15,
});

const littleGallery = document.querySelector(".gallery-little");
const msnrSecond = new Masonry(littleGallery, {
  itemSelector: ".little-item",
  columnWidth: 4,
  gutter: 2,
});
